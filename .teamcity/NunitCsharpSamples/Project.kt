package NunitCsharpSamples

import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.Project

object Project : Project({
    id("NunitCsharpSamples")
    name = "nunit-csharp-samples"
    description = "NUnit tests"
})
