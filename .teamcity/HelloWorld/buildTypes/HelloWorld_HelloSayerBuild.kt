package HelloWorld.buildTypes

import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.VisualStudioStep
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.visualStudio
import jetbrains.buildServer.configs.kotlin.v2018_2.triggers.vcs

object HelloWorld_HelloSayerBuild : BuildType({
    name = "HelloSayer Build"

    vcs {
        root(HelloWorld.vcsRoots.HelloWorld_HttpsGithubComTolacheHelloSayerRefsHeadsMaster)

        cleanCheckout = true
    }

    steps {
        step {
            type = "jb.nuget.installer"
            param("nuget.updatePackages", "true")
            param("nuget.path", "%teamcity.tool.NuGet.CommandLine.DEFAULT%")
            param("nuget.updatePackages.mode", "sln")
            param("sln.path", "HelloSayer.sln")
        }
        visualStudio {
            path = "HelloSayer.sln"
            version = VisualStudioStep.VisualStudioVersion.vs2017
            runPlatform = VisualStudioStep.Platform.x86
            msBuildVersion = VisualStudioStep.MSBuildVersion.V15_0
            msBuildToolsVersion = VisualStudioStep.MSBuildToolsVersion.V15_0
        }
        step {
            type = "jb.nuget.pack"
            param("nuget.pack.output.clean", "true")
            param("nuget.pack.specFile", "HelloSayer/HelloSayer.csproj")
            param("nuget.pack.output.directory", "%teamcity.build.checkoutDir%/HelloSayer/Packages")
            param("nuget.path", "%teamcity.tool.NuGet.CommandLine.4.9.2%")
        }
        step {
            type = "jb.nuget.publish"
            param("secure:nuget.api.key", "credentialsJSON:5a62f44b-68d7-43db-bade-6332ba59ab49")
            param("nuget.path", "%teamcity.tool.NuGet.CommandLine.DEFAULT%")
            param("nuget.publish.source", "https://tolache.pkgs.visualstudio.com/_packaging/HelloSayer_Feed/nuget/v3/index.json")
            param("nuget.publish.files", """HelloSayer\Packages\*.nupkg""")
        }
    }

    triggers {
        vcs {
        }
    }

    features {
        feature {
            type = "jb.nuget.auth"
            param("nuget.auth.feed", "https://tolache.pkgs.visualstudio.com/_packaging/HelloSayer_Feed/nuget/v3/index.json")
            param("secure:nuget.auth.password", "credentialsJSON:5a62f44b-68d7-43db-bade-6332ba59ab49")
            param("nuget.auth.username", "UNIT-905")
        }
    }
})
