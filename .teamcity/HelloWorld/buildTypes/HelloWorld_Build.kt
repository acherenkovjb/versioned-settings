package HelloWorld.buildTypes

import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.VisualStudioStep
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.visualStudio
import jetbrains.buildServer.configs.kotlin.v2018_2.triggers.vcs

object HelloWorld_Build : BuildType({
    name = "Hello_World Build"

    artifactRules = """
        Hello_World\Hello_World\bin\Debug\Hello_World.exe
        Hello_World\Hello_World\bin\Debug\Hello_World.pdb
        Hello_World\Hello_World\bin\Debug\HelloSayer.dll
        Hello_World\packages\HelloSayer*\HelloSayer*.nupkg
    """.trimIndent()

    vcs {
        root(HelloWorld.vcsRoots.HelloWorld_HttpsGithubComTolacheHelloWorldRefsHeadsMaster)

        cleanCheckout = true
    }

    steps {
        step {
            type = "jb.nuget.installer"
            param("nuget.updatePackages", "true")
            param("nuget.path", "%teamcity.tool.NuGet.CommandLine.5.1.0%")
            param("nuget.sources", """
                https://api.nuget.org/v3/index.json
                https://tolache.pkgs.visualstudio.com/_packaging/HelloSayer_Feed/nuget/v3/index.json
            """.trimIndent())
            param("nuget.updatePackages.mode", "perConfig")
            param("sln.path", "Hello_World/Hello_World.sln")
            param("nuget.noCache", "true")
            param("nuget.updatePackages.include.prerelease", "true")
        }
        visualStudio {
            name = "Hello_World"
            path = "Hello_World/Hello_World.sln"
            version = VisualStudioStep.VisualStudioVersion.vs2017
            runPlatform = VisualStudioStep.Platform.x86
            msBuildVersion = VisualStudioStep.MSBuildVersion.V15_0
            msBuildToolsVersion = VisualStudioStep.MSBuildToolsVersion.V15_0
        }
    }

    triggers {
        vcs {
        }
        trigger {
            type = "nuget.simple"
            param("nuget.include.prerelease", "true")
            param("nuget.source", "http://UNIT-905.Labs.IntelliJ.Net:8110/httpAuth/app/nuget/feed/HelloWorld/LocalNuGetFeed/v2")
            param("nuget.exe", "%teamcity.tool.NuGet.CommandLine.DEFAULT%")
            param("nuget.username", "admin")
            param("secure:nuget.password", "credentialsJSON:eca3d0a1-0303-4a95-bc66-f386c7b79571")
            param("nuget.package", "HelloSayer")
        }
    }

    features {
        feature {
            type = "jb.nuget.auth"
            param("nuget.auth.feed", "https://tolache.pkgs.visualstudio.com/_packaging/HelloSayer_Feed/nuget/v3/index.json")
            param("secure:nuget.auth.password", "credentialsJSON:5a62f44b-68d7-43db-bade-6332ba59ab49")
            param("nuget.auth.username", "UNIT-905")
        }
    }
})
