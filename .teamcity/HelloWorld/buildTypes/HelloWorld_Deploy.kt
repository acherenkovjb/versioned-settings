package HelloWorld.buildTypes

import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.exec

object HelloWorld_Deploy : BuildType({
    name = "Deploy"

    vcs {
        cleanCheckout = true
    }

    steps {
        exec {
            name = "Copy file"
            path = "cp"
            arguments = """C:\Temp\ C:\Deploy\ -r"""
        }

        exec {
            name = "Say hi"
            path = "echo"
            arguments = "Hi!"
        }
    }

    dependencies {
        dependency(HelloWorld_Build) {
            snapshot {
                reuseBuilds = ReuseBuilds.NO
            }

            artifacts {
                artifactRules = """**\HelloSayer*.nupkg => C:\Temp"""
            }
        }
    }
})
