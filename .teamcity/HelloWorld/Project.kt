package HelloWorld

import HelloWorld.buildTypes.*
import HelloWorld.vcsRoots.*
import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.Project
import jetbrains.buildServer.configs.kotlin.v2018_2.projectFeatures.nuGetFeed

object Project : Project({
    id("HelloWorld")
    name = "Hello_World"

    vcsRoot(HelloWorld_HttpsGithubComTolachePowerShellTestRefsHeadsMaster)
    vcsRoot(HelloWorld_HttpsGithubComTolacheHelloSayerRefsHeadsMaster)
    vcsRoot(HelloWorld_HttpsGithubComTolacheHelloWorldRefsHeadsMaster)

    buildType(HelloWorld_HelloSayerBuild)
    buildType(HelloWorld_Deploy)
    buildType(HelloWorld_Build)

    features {
        feature {
            id = "PROJECT_EXT_3"
            type = "project-graphs"
            param("series", """
                [
                  {
                    "type": "valueType",
                    "title": "Build Duration (all stages)",
                    "sourceBuildTypeId": "HelloWorld_Build",
                    "key": "BuildDuration"
                  }
                ]
            """.trimIndent())
            param("format", "text")
            param("hideFilters", "")
            param("title", "Hello_World Build Duration")
            param("defaultFilters", "")
            param("seriesTitle", "Serie")
        }
        nuGetFeed {
            id = "repository-nuget-LocalNuGetFeed"
            name = "LocalNuGetFeed"
            description = "Local NuGet Feed"
        }
    }
    buildTypesOrder = arrayListOf(HelloWorld_HelloSayerBuild, HelloWorld_Build)
})
