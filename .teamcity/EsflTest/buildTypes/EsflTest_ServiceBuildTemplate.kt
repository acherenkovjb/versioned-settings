package EsflTest.buildTypes

import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.maven
import jetbrains.buildServer.configs.kotlin.v2018_2.triggers.mavenSnapshot

object EsflTest_ServiceBuildTemplate : Template({
    name = "service_build_template"

    params {
        param("system.maven_repo_snapshot_ci", "https://nexus-ci.int.gazprombank.ru/repository/esfl-maven-snapshot/")
        param("system.maven_repo_release_ci", "https://nexus-ci.int.gazprombank.ru/repository/esfl-maven-lib/")
    }

    steps {
        maven {
            name = "Purge local repo"
            id = "RUNNER_128"
            goals = "org.apache.maven.plugins:maven-dependency-plugin:3.1.1:purge-local-repository"
            runnerArgs = "-X"
            mavenVersion = bundled_3_5()
            userSettingsSelection = "settings-gpb.xml"
            useOwnLocalRepo = true
            jdkHome = "%env.JDK_18_x64%"
        }
        maven {
            name = "Deploy to Nexus"
            id = "RUNNER_129"
            goals = "org.apache.maven.plugins:maven-clean-plugin:3.1.0:clean org.apache.maven.plugins:maven-resources-plugin:3.1.0:resources deploy"
            runnerArgs = "-X -U -DaltReleaseDeploymentRepository=esfl-maven-lib::default::%system.maven_repo_release_ci% -DaltSnapshotDeploymentRepository=esfl-maven-snapshot::default::%system.maven_repo_snapshot_ci%"
            mavenVersion = bundled_3_5()
            userSettingsSelection = "settings-gpb.xml"
            useOwnLocalRepo = true
        }
    }

    triggers {
        mavenSnapshot {
            id = "mavenSnapshotDependencyTrigger"
            skipIfRunning = true
        }
    }
})
