package EsflTest.buildTypes

import EsflTest.vcsRoots.EsflTest_mavensamples2
import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.maven
import jetbrains.buildServer.configs.kotlin.v2018_2.triggers.mavenSnapshot
import jetbrains.buildServer.configs.kotlin.v2018_2.triggers.vcs

object EsflTest_MessagingCommonsTestTc : BuildType({
    name = "messaging-commons-test-TC"
    description = "messaging-commons to nexus"

    params {
        param("system.maven_repo_snapshot_ci", "https://nexus-ci.int.gazprombank.ru/repository/esfl-maven-snapshot/")
        param("system.maven_repo_release_ci", "https://nexus-ci.int.gazprombank.ru/repository/esfl-maven-lib/")
    }

    vcs {
        root(EsflTest.vcsRoots.EsflTest_mavensamples, "+:. => 1")
        root(EsflTest.vcsRoots.EsflTest_mavensamples2, "+:. => 2")
    }

    steps {
        maven {
            name = "Purge local repo"
            goals = "org.apache.maven.plugins:maven-dependency-plugin:3.1.1:purge-local-repository"
            pomLocation = "2/pom.xml"
            runnerArgs = "-X"
            workingDir = "2"
            mavenVersion = bundled_3_5()
            userSettingsSelection = "settings-gpb.xml"
            jdkHome = "%env.JDK_18_x64%"
        }
        maven {
            name = "Deploy to Nexus"
            goals = "org.apache.maven.plugins:maven-clean-plugin:3.1.0:clean org.apache.maven.plugins:maven-resources-plugin:3.1.0:resources deploy"
            pomLocation = "2/pom.xml"
            runnerArgs = "-X -U -DaltReleaseDeploymentRepository=esfl-maven-lib::default::%system.maven_repo_release_ci% -DaltSnapshotDeploymentRepository=esfl-maven-snapshot::default::%system.maven_repo_snapshot_ci%"
            workingDir = "2"
            mavenVersion = bundled_3_5()
            userSettingsSelection = "settings-gpb.xml"
            jdkHome = "%env.JDK_18_x64%"
        }
    }

    triggers {
        vcs {
            triggerRules = "+:root=${EsflTest_mavensamples2.id}:**"

            branchFilter = ""
        }
        mavenSnapshot {
            skipIfRunning = true
        }
    }
})
