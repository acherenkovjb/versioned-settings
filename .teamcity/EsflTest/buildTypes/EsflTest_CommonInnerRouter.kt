package EsflTest.buildTypes

import EsflTest.vcsRoots.EsflTest_mavensamples
import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.maven
import jetbrains.buildServer.configs.kotlin.v2018_2.triggers.vcs

object EsflTest_CommonInnerRouter : BuildType({
    templates(EsflTest_ServiceBuildTemplate)
    name = "common-inner-router"
    description = "common-inner-router to nexus"

    vcs {
        root(EsflTest.vcsRoots.EsflTest_mavensamples, "+:. => 1")
        root(EsflTest.vcsRoots.EsflTest_mavensamples2, "+:. => 2")
    }

    steps {
        maven {
            name = "Purge local repo"
            id = "RUNNER_128"
            goals = "org.apache.maven.plugins:maven-dependency-plugin:3.1.1:purge-local-repository"
            pomLocation = "1/pom.xml"
            runnerArgs = "-X"
            workingDir = "1"
            mavenVersion = bundled_3_5()
            userSettingsSelection = "settings-gpb.xml"
            useOwnLocalRepo = true
            jdkHome = "%env.JDK_18_x64%"
        }
        maven {
            name = "Deploy to Nexus"
            id = "RUNNER_129"
            goals = "org.apache.maven.plugins:maven-clean-plugin:3.1.0:clean org.apache.maven.plugins:maven-resources-plugin:3.1.0:resources deploy"
            pomLocation = "1/pom.xml"
            runnerArgs = "-X -U -DaltReleaseDeploymentRepository=esfl-maven-lib::default::%system.maven_repo_release_ci% -DaltSnapshotDeploymentRepository=esfl-maven-snapshot::default::%system.maven_repo_snapshot_ci%"
            workingDir = "1"
            mavenVersion = bundled_3_5()
            userSettingsSelection = "settings-gpb.xml"
            useOwnLocalRepo = true
        }
    }

    triggers {
        vcs {
            id = "vcsTrigger"
            triggerRules = "+:root=${EsflTest_mavensamples.id}:**"

            branchFilter = ""
        }
    }
})
