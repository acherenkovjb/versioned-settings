package EsflTest

import EsflTest.buildTypes.*
import EsflTest.vcsRoots.*
import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.Project

object Project : Project({
    id("EsflTest")
    name = "ESFL_TEST"
    description = "Пример ошибки Maven-триггера"

    vcsRoot(EsflTest_mavensamples)
    vcsRoot(EsflTest_mavensamples2)

    buildType(EsflTest_MessagingCommonsTestTc)
    buildType(EsflTest_CommonInnerRouter)

    template(EsflTest_ServiceBuildTemplate)

    features {
        feature {
            id = "PROJECT_EXT_2"
            type = "OAuthProvider"
            param("clientId", "PwVeZAcatQTca5EuU9")
            param("secure:clientSecret", "credentialsJSON:39ffc61a-ec55-4256-a7b8-cd2c04a95843")
            param("displayName", "Bitbucket Cloud")
            param("providerType", "BitBucketCloud")
        }
    }
})
