package EsflTest.vcsRoots

import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.vcs.GitVcsRoot

object EsflTest_mavensamples : GitVcsRoot({
    name = "maven-samples"
    url = "https://bitbucket.org/acherenkovjb/maven-samples/src/master/"
    authMethod = password {
        userName = "anatoly.cherenkov@jetbrains.com"
        password = "credentialsJSON:fce723e9-6b95-4c8f-9125-2714cf9afc6e"
    }
})
