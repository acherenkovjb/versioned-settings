package EsflTest.vcsRoots

import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.vcs.GitVcsRoot

object EsflTest_mavensamples2 : GitVcsRoot({
    name = "maven-samples2"
    url = "https://bitbucket.org/acherenkovjb/maven-samples2/src/master/"
    authMethod = password {
        userName = "anatoly.cherenkov@jetbrains.com"
        password = "credentialsJSON:fce723e9-6b95-4c8f-9125-2714cf9afc6e"
    }
})
