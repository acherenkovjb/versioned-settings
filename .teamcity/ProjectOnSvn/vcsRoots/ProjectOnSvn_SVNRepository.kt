package ProjectOnSvn.vcsRoots

import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.vcs.SvnVcsRoot

object ProjectOnSvn_SVNRepository : SvnVcsRoot({
    name = "BankSVN"
    url = "https://UNIT-905.Labs.IntelliJ.Net:8443/svn/BankSVN/"
    userName = "SVNuser"
    password = "credentialsJSON:9c62d6f1-2525-4c97-bf54-422c88f6e737"
    configDir = """C:\Windows\system32\config\systemprofile\AppData\Roaming\Subversion"""
    enableNonTrustedSSL = true
})
