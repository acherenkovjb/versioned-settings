package ProjectOnSvn.buildTypes

import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.VisualStudioStep
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.visualStudio
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.vstest

object ProjectOnSvn_Bank : BuildType({
    name = "Bank"

    params {
        param("system.command.line.run.interpreter", "false")
        param("teamcity.svn.native.enabled", "true")
    }

    vcs {
        root(ProjectOnSvn.vcsRoots.ProjectOnSvn_SVNRepository, "+:trunk => .")

        checkoutMode = CheckoutMode.ON_AGENT
        cleanCheckout = true
    }

    steps {
        step {
            type = "jb.nuget.installer"
            param("nuget.path", "%teamcity.tool.NuGet.CommandLine.DEFAULT%")
            param("nuget.updatePackages.mode", "sln")
            param("sln.path", "Bank.sln")
        }
        visualStudio {
            path = "Bank.sln"
            version = VisualStudioStep.VisualStudioVersion.vs2017
            runPlatform = VisualStudioStep.Platform.x86
            msBuildVersion = VisualStudioStep.MSBuildVersion.V15_0
            msBuildToolsVersion = VisualStudioStep.MSBuildToolsVersion.V15_0
        }
        vstest {
            vstestPath = "%teamcity.dotnet.vstest.15.0%"
            includeTestFileNames = """BankTest\bin\Debug\BankTest.dll"""
            coverage = dotcover {
                toolPath = "%teamcity.tool.JetBrains.dotCover.CommandLineTools.DEFAULT%"
            }
        }
    }
})
