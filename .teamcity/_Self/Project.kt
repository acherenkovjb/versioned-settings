package _Self

import _Self.vcsRoots.*
import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.Project

object Project : Project({
    description = "Contains all other projects"

    vcsRoot(PerforceLinuxConfig)

    features {
        feature {
            id = "PROJECT_EXT_1"
            type = "ReportTab"
            param("startPage", "coverage.zip!index.html")
            param("title", "Code Coverage")
            param("type", "BuildReportTab")
        }
        feature {
            id = "PROJECT_EXT_9"
            type = "OAuthProvider"
            param("clientId", "Rpssj7KGS2heqeZnbj")
            param("secure:clientSecret", "credentialsJSON:44acb5a1-f4a4-4c42-bd26-c976cf3faa7e")
            param("displayName", "Bitbucket Cloud")
            param("providerType", "BitBucketCloud")
        }
    }

    cleanup {
        preventDependencyCleanup = false
    }

    subProject(NunitCsharpSamples.Project)
    subProject(EsflTest.Project)
    subProject(ProjectOnSvn.Project)
    subProject(TestbedAntSample.Project)
    subProject(id2011012JunitInspection.Project)
    subProject(PowerShellTest.Project)
    subProject(HelloWorld.Project)
})
