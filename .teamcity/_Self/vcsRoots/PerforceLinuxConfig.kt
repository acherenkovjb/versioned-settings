package _Self.vcsRoots

import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.vcs.PerforceVcsRoot

object PerforceLinuxConfig : PerforceVcsRoot({
    name = "perforceLinuxConfig"
    port = "ssl:172.20.186.72:1666"
    mode = stream {
        streamName = "//mydepot/perforceLinuxConfig"
    }
    userName = "Anatoly.Cherenkov"
    workspaceOptions = """
        Options:        noallwrite clobber nocompress unlocked nomodtime rmdir
        Host:           %teamcity.agent.hostname%
        SubmitOptions:  revertunchanged
        LineEnd:        local
    """.trimIndent()
})
