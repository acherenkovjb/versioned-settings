package PowerShellTest

import PowerShellTest.buildTypes.*
import PowerShellTest.vcsRoots.*
import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.Project

object Project : Project({
    id("PowerShellTest")
    name = "PowerShellTest"
    archived = true

    vcsRoot(PowerShellTest_HttpsGithubComTolachePowerShellTestRefsHeadsMaster)

    buildType(PowerShellTest_Build)
})
