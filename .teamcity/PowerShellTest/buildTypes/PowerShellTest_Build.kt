package PowerShellTest.buildTypes

import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.powerShell
import jetbrains.buildServer.configs.kotlin.v2018_2.triggers.vcs

object PowerShellTest_Build : BuildType({
    name = "Build"
    paused = true

    artifactRules = """
        TestScript.ps1
        TestFile.dat
    """.trimIndent()

    vcs {
        root(PowerShellTest.vcsRoots.PowerShellTest_HttpsGithubComTolachePowerShellTestRefsHeadsMaster)

        cleanCheckout = true
    }

    steps {
        powerShell {
            scriptMode = file {
                path = "TestScript.ps1"
            }
            noProfile = false
        }
    }

    triggers {
        vcs {
        }
    }

    cleanup {
        artifacts(builds = 2)
    }
})
