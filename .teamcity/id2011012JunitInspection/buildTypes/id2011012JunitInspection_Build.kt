package id2011012JunitInspection.buildTypes

import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.ant
import jetbrains.buildServer.configs.kotlin.v2018_2.triggers.vcs

object id2011012JunitInspection_Build : BuildType({
    name = "Build"

    vcs {
        root(id2011012JunitInspection.vcsRoots.id2011012JunitInspection_HttpsGithubComTolache2011012junitInspectionRefsHeadsMas)
    }

    steps {
        ant {
            mode = antFile {
            }
            targets = "unittest"
        }
    }

    triggers {
        vcs {
        }
    }
})
