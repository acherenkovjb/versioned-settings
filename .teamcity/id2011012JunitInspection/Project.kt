package id2011012JunitInspection

import id2011012JunitInspection.buildTypes.*
import id2011012JunitInspection.vcsRoots.*
import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.Project

object Project : Project({
    id("id2011012JunitInspection")
    name = "2011012 Junit Inspection"

    vcsRoot(id2011012JunitInspection_HttpsGithubComTolache2011012junitInspectionRefsHeadsMas)

    buildType(id2011012JunitInspection_Build)
})
