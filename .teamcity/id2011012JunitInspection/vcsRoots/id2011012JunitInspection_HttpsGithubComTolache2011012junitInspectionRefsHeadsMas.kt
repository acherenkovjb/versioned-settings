package id2011012JunitInspection.vcsRoots

import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.vcs.GitVcsRoot

object id2011012JunitInspection_HttpsGithubComTolache2011012junitInspectionRefsHeadsMas : GitVcsRoot({
    name = "https://github.com/tolache/2011012-junit-inspection#refs/heads/master"
    url = "https://github.com/tolache/2011012-junit-inspection"
    authMethod = password {
        userName = "tolache"
        password = "credentialsJSON:7d8cca8e-bc35-4156-a965-0b32123691bc"
    }
})
