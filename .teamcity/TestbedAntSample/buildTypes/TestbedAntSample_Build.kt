package TestbedAntSample.buildTypes

import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.ant
import jetbrains.buildServer.configs.kotlin.v2018_2.triggers.vcs

object TestbedAntSample_Build : BuildType({
    name = "Build"

    vcs {
        root(TestbedAntSample.vcsRoots.TestbedAntSample_HttpsGithubComTolacheTestbedAntSampleRefsHeadsMaster)
    }

    steps {
        ant {
            mode = antFile {
            }
            targets = "all"
        }
    }

    triggers {
        vcs {
        }
    }
})
